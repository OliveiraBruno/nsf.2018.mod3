﻿use CursoDB;

insert into tb_curso (nm_curso, qt_horas)
	 values ('Informática', 1200),
            ('Administração', 1000),
            ('Comunicação Visual', 900),
            ('Secretariado', 950),
            ('Mecânica de Auto', 1050),
            ('Eletrotécnica', 1100),
            ('Inglês', 400);
            

insert into tb_turma (nm_turma, id_curso, qt_alunos)
	 values ('InfoA', 1, 40),
			('InfoB', 1, 30),
            ('InfoC', 1, 45),
            ('InfoD', 1, 35),
            ('AdmA', 2, 30),
            ('AdmB', 2, 32),
            ('AdmC', 2, 35),
            ('AdmD', 2, 36),
            ('AdmE', 2, 34),
            ('AdmF', 2, 33),
            ('AdmG', 2, 32),
            ('CVA', 3, 48),
            ('CVB', 3, 46),
            ('SecA', 4, 43),
            ('SecB', 4, 42),
            ('MecA', 5, 41),
            ('MecB', 5, 43),
            ('EleA', 6, 35),
            ('EleB', 6, 36),
            ('IngA-Z', 7, 1000000)
			;
     
