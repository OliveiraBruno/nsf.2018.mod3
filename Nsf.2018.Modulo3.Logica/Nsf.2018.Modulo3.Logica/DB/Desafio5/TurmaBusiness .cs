﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio5
{
    class TurmaBusiness
    {
        public List<TurmaDTO> Consultar(int idCurso)
        {
            TurmaDatabase db = new TurmaDatabase();
            return db.Consultar(idCurso);
        }
    }
}
