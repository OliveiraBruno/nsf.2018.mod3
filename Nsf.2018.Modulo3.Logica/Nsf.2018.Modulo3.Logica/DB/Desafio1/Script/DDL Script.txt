﻿drop database ViagemDB;
create database ViagemDB;
use ViagemDB;

create table tb_viagem (
	id_viagem 		int primary key auto_increment,
    nm_viagem 		varchar(100),
    vl_passagem		decimal(15,2),
    vl_hotel_adulto_dia		decimal(15,2),
    vl_hotel_crianca_dia	decimal(15,2)
);

