﻿using Nsf._2018.Modulo3.Logica.DB.Desafio2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio2 : Form
    {
        public frmDesafio2()
        {
            InitializeComponent();
        }

        private void btnIistar_Click(object sender, EventArgs e)
        {
            IngressoBusiness bus = new IngressoBusiness();
            List<IngressoDTO> lista = bus.Listar();

            dgvIngressos.AutoGenerateColumns = false;
            dgvIngressos.DataSource = lista;
        }

        private void btnReservar_Click(object sender, EventArgs e)
        {
            // Percorre as linhas que foram selecionadas na Grid
            foreach (DataGridViewRow item in dgvIngressos.SelectedRows)
            {
                // Transforma a linha selecionada em DTO
                IngressoDTO dto = item.DataBoundItem as IngressoDTO;

                // Chama a função Reservar
                Reservar(dto);
            }
        }
        
        private void Reservar(IngressoDTO ingressoDto)
        {
            // Se o Ingresso está reservado, avisa o usuário
            if (ingressoDto.Reservado == true)
            {
                MessageBox.Show($"Ingresso {ingressoDto.Lugar} já foi reservado.", 
                    "Ingresso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            // Senão, altera seu estado para reservado e altera no banco de dados
            else
            {
                ingressoDto.Reservado = true;

                IngressoBusiness business = new IngressoBusiness();
                business.Alterar(ingressoDto);

                MessageBox.Show($"Ingresso {ingressoDto.Lugar}({ingressoDto.Nome}) reservado com sucesso.",
                    "Ingresso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }



        private void btnCancelar_Click(object sender, EventArgs e)
        {
            // Percorre as linhas que foram selecionadas na Grid
            foreach (DataGridViewRow item in dgvIngressos.SelectedRows)
            {
                // Transforma a linha selecionada em DTO
                IngressoDTO dto = item.DataBoundItem as IngressoDTO;

                // Chama a função Cancelar
                Cancelar(dto);
            }
        }

        private void Cancelar(IngressoDTO ingressoDto)
        {
            ingressoDto.Reservado = false;

            IngressoBusiness business = new IngressoBusiness();
            business.Alterar(ingressoDto);

            MessageBox.Show($"Ingresso {ingressoDto.Lugar}({ingressoDto.Nome}) cancelado com sucesso.",
                "Ingresso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        
    }
}
