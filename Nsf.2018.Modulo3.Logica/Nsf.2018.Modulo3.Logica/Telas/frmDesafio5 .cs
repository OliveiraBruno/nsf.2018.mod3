﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using Nsf._2018.Modulo3.Logica.DB.Desafio5;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio5 : Form
    {
        public frmDesafio5()
        {
            InitializeComponent();
            CarregarCursos();
        }

        private void CarregarCursos()
        {
            CursoBusiness bus = new CursoBusiness();
            List<CursoDTO> lista = bus.Listar();

            cboCurso.ValueMember = nameof(CursoDTO.Id);
            cboCurso.DisplayMember = nameof(CursoDTO.Nome);
            cboCurso.DataSource = lista;
        }

        private void CarregarTurma(int idCurso)
        {
            TurmaBusiness bus = new TurmaBusiness();
            List<TurmaDTO> lista = bus.Consultar(idCurso);

            cboTurma.ValueMember = nameof(TurmaDTO.Id);
            cboTurma.DisplayMember = nameof(TurmaDTO.Nome);
            cboTurma.DataSource = lista;
        }


        private void cboCurso_SelectedIndexChanged(object sender, EventArgs e)
        {
            CursoDTO curso = cboCurso.SelectedItem as CursoDTO;
            CarregarTurma(curso.Id);
        }

        private void cboTurma_SelectedIndexChanged(object sender, EventArgs e)
        {
            CursoDTO curso = cboCurso.SelectedItem as CursoDTO;
            TurmaDTO turma = cboTurma.SelectedItem as TurmaDTO;

            lblTotalHoras.Text = curso.QtdHoras.ToString();
            lblTotalAlunos.Text = turma.TotalAlunos.ToString();

        }
    }
}
